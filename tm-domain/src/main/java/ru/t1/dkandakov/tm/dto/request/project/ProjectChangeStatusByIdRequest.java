package ru.t1.dkandakov.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.request.AbstractIdRequest;
import ru.t1.dkandakov.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectChangeStatusByIdRequest extends AbstractIdRequest {

    @Nullable
    private Status status;

    public ProjectChangeStatusByIdRequest(@Nullable final String id, @Nullable final Status status) {
        super(id);
        this.status = status;
    }

}

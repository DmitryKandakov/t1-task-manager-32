package ru.t1.dkandakov.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class UserLoginRequest extends AbstractUserRequest {

    @NotNull
    private String login;

    @Nullable
    private String password;

    public UserLoginRequest(@NotNull String login, @Nullable String password) {
        this.login = login;
        this.password = password;
    }

}

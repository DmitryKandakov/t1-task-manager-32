package ru.t1.dkandakov.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.request.AbstractIdRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskGetByIdRequest extends AbstractIdRequest {

    public TaskGetByIdRequest(@Nullable String id) {
        super(id);
    }
}

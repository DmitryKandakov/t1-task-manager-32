package ru.t1.dkandakov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkandakov.tm.dto.request.system.ServerAboutRequest;
import ru.t1.dkandakov.tm.dto.request.system.ServerVersionRequest;
import ru.t1.dkandakov.tm.dto.response.system.ServerAboutResponse;
import ru.t1.dkandakov.tm.dto.response.system.ServerVersionResponse;

public interface ISystemEndpoint {

    @NotNull
    ServerAboutResponse getAbout(@NotNull ServerAboutRequest request);

    @NotNull
    ServerVersionResponse getVersion(@NotNull ServerVersionRequest request);

}
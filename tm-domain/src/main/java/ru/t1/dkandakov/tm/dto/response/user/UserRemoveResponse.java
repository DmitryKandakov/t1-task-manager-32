package ru.t1.dkandakov.tm.dto.response.user;

import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.response.AbstractUserResponse;
import ru.t1.dkandakov.tm.model.User;

public final class UserRemoveResponse extends AbstractUserResponse {

    public UserRemoveResponse(@Nullable final User user) {
        super(user);
    }

}

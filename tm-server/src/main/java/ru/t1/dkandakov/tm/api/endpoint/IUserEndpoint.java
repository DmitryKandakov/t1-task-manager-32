package ru.t1.dkandakov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkandakov.tm.dto.request.user.*;
import ru.t1.dkandakov.tm.dto.response.user.*;

public interface IUserEndpoint {

    @NotNull UserLockResponse lockUser(@NotNull UserLockRequest request);

    @NotNull UserUnlockResponse unlockUser(@NotNull UserUnlockRequest request);

    @NotNull UserRemoveResponse removeUser(@NotNull UserRemoveRequest request);

    @NotNull UserUpdateProfileResponse updateUserProfile(@NotNull UserUpdateProfileRequest request);

    @NotNull UserChangePasswordResponse changeUserPassword(@NotNull UserChangePasswordRequest request);

    @NotNull UserRegistryResponse registryUser(@NotNull UserRegistryRequest request);

    @NotNull UserProfileResponse viewUserProfile(@NotNull UserProfileRequest request);

}

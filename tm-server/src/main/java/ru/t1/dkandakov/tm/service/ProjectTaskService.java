package ru.t1.dkandakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.api.repository.IProjectRepository;
import ru.t1.dkandakov.tm.api.repository.ITaskRepository;
import ru.t1.dkandakov.tm.api.service.IProjectTaskService;
import ru.t1.dkandakov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dkandakov.tm.exception.entity.TaskNotFoundException;
import ru.t1.dkandakov.tm.exception.field.IndexIncorrectException;
import ru.t1.dkandakov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.dkandakov.tm.exception.field.TaskIdEmptyException;
import ru.t1.dkandakov.tm.exception.field.UserIdEmptyException;
import ru.t1.dkandakov.tm.model.Project;
import ru.t1.dkandakov.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    public ProjectTaskService(
            @NotNull final IProjectRepository projectRepository,
            @NotNull final ITaskRepository taskRepository
    ) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        @Nullable final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
    }

    @Override
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
        for (@NotNull final Task task : tasks) taskRepository.removeOneById(userId, task.getId());
        projectRepository.removeOneById(userId, projectId);
    }

    @Override
    public void removeProjectByIndex(@Nullable final String userId, @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= projectRepository.getSize()) throw new IndexIncorrectException();
        @NotNull final Project project = projectRepository.findOneByIndex(userId, index);
        removeProjectById(userId, project.getId());
    }

    @Override
    public void unbindTaskFromProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
    }

}
package ru.t1.dkandakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.api.repository.IRepository;
import ru.t1.dkandakov.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final List<M> records = new ArrayList<>();

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        records.add(model);
        return model;
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        this.records.addAll(models);
        return models;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        clear();
        return add(models);
    }

    @Nullable
    @Override
    public List<M> findAll() {
        return records;
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        @NotNull final List<M> result = new ArrayList<>(records);
        result.sort(comparator);
        return result;
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) {
        return records
                .stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final Integer index) {
        return records.get(index);
    }

    @Nullable
    @Override
    public M removeOne(@Nullable final M model) {
        records.remove(model);
        return model;
    }

    @Nullable
    @Override
    public M removeOneById(@NotNull final String id) {
        @Nullable final M model = findOneById(id);
        if (model == null) return null;
        records.remove(model);
        return model;
    }

    @Nullable
    @Override
    public M removeOneByIndex(@NotNull final Integer index) {
        @Nullable final M model = findOneByIndex(index);
        if (model == null) return null;
        records.remove(model);
        return model;
    }

    @Override
    public void removeAll(Collection<M> collection) {
        records.clear();
    }

    @Override
    public void clear() {
        records.clear();
    }

    @Override
    public int getSize() {
        return records.size();
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

}
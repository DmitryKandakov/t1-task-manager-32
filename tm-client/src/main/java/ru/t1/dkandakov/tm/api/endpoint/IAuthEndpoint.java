package ru.t1.dkandakov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkandakov.tm.dto.request.user.UserLoginRequest;
import ru.t1.dkandakov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.dkandakov.tm.dto.request.user.UserProfileRequest;
import ru.t1.dkandakov.tm.dto.response.user.UserLoginResponse;
import ru.t1.dkandakov.tm.dto.response.user.UserLogoutResponse;
import ru.t1.dkandakov.tm.dto.response.user.UserProfileResponse;

public interface IAuthEndpoint extends IEndpointClient {

    @NotNull
    UserLoginResponse login(@NotNull UserLoginRequest request);

    @NotNull
    UserLogoutResponse logout(@NotNull UserLogoutRequest request);

    @NotNull
    UserProfileResponse profile(@NotNull UserProfileRequest request);

}

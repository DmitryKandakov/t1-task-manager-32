package ru.t1.dkandakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.request.project.ProjectListRequest;
import ru.t1.dkandakov.tm.enumerated.ProjectSort;
import ru.t1.dkandakov.tm.model.Project;
import ru.t1.dkandakov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectShowProjectsCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT");
        System.out.println(Arrays.toString(ProjectSort.values()));
        @Nullable final String sortType = TerminalUtil.nextLine();
        @Nullable final ProjectSort projectSort = ProjectSort.toSort(sortType);

        @NotNull final ProjectListRequest request = new ProjectListRequest(projectSort);
        @Nullable final List<Project> projects = getProjectEndpoint().listProject(request).getProjects();
        int index = 1;
        for (@Nullable final Project project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project.getName() + " : " + project.getDescription());
            index++;
        }
    }

    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show project list.";
    }

}
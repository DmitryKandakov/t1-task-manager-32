package ru.t1.dkandakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.request.domain.DataXmlLoadFasterXmlRequest;
import ru.t1.dkandakov.tm.enumerated.Role;

public class DataXmlLoadFasterXmlCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public @Nullable String getDescription() {
        return "Load data from xml file";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[LOAD XML DATA FASTER]");
        getDomainEndpoint().loadDataXmlFasterXml(new DataXmlLoadFasterXmlRequest());
    }

    @Nullable
    @Override
    public String getName() {
        return "data-load-xml";
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
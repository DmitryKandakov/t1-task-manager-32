package ru.t1.dkandakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.request.domain.DataJsonSaveFasterXmlRequest;
import ru.t1.dkandakov.tm.enumerated.Role;

public class DataJsonSaveFasterXmlCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public String getDescription() {
        return "Save data in json file";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[SAVE JSON DATA FASTER]");
        getDomainEndpoint().saveDataJsonFasterXml(new DataJsonSaveFasterXmlRequest());
    }

    @Nullable
    @Override
    public String getName() {
        return "data-save-json";
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}

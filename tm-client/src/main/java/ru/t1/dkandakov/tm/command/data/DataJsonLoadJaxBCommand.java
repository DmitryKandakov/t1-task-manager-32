package ru.t1.dkandakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.request.domain.DataJsonLoadJaxBRequest;
import ru.t1.dkandakov.tm.enumerated.Role;

public class DataJsonLoadJaxBCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load data from json file";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[LOAD JSON DATA]");
        getDomainEndpoint().loadDataJsonJaxb(new DataJsonLoadJaxBRequest());
    }


    @Nullable
    @Override
    public String getName() {
        return "data-load-json-jaxb";
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
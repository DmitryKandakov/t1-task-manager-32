package ru.t1.dkandakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.request.domain.DataXmlSaveJaxBRequest;
import ru.t1.dkandakov.tm.enumerated.Role;

public class DataXmlSaveJaxBCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save data in xml file";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[SAVE XML DATA]");
        getDomainEndpoint().saveDataXmlJaxb(new DataXmlSaveJaxBRequest());
    }

    @NotNull
    @Override
    public String getName() {
        return "data-save-xml-jaxb";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}